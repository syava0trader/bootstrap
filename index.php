<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">
    <link href='style.css' rel='stylesheet'>
    <title>Document</title>
</head>
<body>
<section >
    <form class="row g-3" action="index.php" method="post">
        <div class="col-md-6">
            <label for="inputEmail4" class="form-label">Email</label>
            <input type="email" name="email" class="form-control" id="inputEmail4">
        </div>
        <div class="col-md-6">
            <label for="inputPassword4" class="form-label">Password</label>
            <input type="password" name="pass" class="form-control" id="inputPassword4">
        </div>
        <div class="col-12">
            <label for="inputAddress" class="form-label">Address</label>
            <input type="text" name="address1" class="form-control" id="inputAddress" placeholder="1234 Main St">
        </div>
        <div class="col-12">
            <label for="inputAddress2" class="form-label">Address 2</label>
            <input type="text" name="address2" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
        </div>
        <div class="col-md-6">
            <label for="inputCity" class="form-label">City</label>
            <input type="text" name="city" class="form-control" id="inputCity">
        </div>
        <div class="col-md-4">
            <label for="inputState" class="form-label">State</label>
            <select name="state" id="inputState" class="form-select">
                <option selected>UA</option>
                <option>USA</option>
                <option>UK</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="inputZip" class="form-label">Zip</label>
            <input name="zip" type="text" class="form-control" id="inputZip">
        </div>
        <div class="col-12">
            <div class="form-check">
                <input name="check" class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Check me out
                </label>
            </div>
        </div>
        <div class="col-12">
            <button type="submit" name="submit" class="btn btn-primary" id="submit">Sign in</button>
        </div>
    </form>
</section>
<section id="answer">
    <?= 'You have entered your e-mail :'.$_POST['email'].'<br>'?>
    <?= 'An your password is:'.$_POST['pass'].'<br>'?>
    <?= 'We are going to send you a gift at your address - '.$_POST['address1'].'<br>'?>
    <?= 'And another gift will be sent at address - '.$_POST['address2'].'<br>'?>
    <?= 'Your City - '.$_POST['city'].'<br>'?>
    <?= 'Your State - '.$_POST['state'].'<br>'?>
    <?= 'Your Zip - '.$_POST['zip'].'<br>'?>
    <?= $_POST['check'] == 'on'? 'Checked <br>': 'Not checked <br>'?>
</section>
<section>
    <img src="HTML_CSS.jpg" alt="My courses rates!">
</section>
<?php include "../2/test.html"?>
</body>
</html>
