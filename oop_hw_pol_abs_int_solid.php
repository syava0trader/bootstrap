<?php

abstract class Transport
{

    protected $type;

    abstract function setTransportType($type);

    abstract function checkTicket();

}

class Person
{
    protected $FIO;

    function setFio($fio)
    {
        $this->FIO = $fio;
    }

    function getFio()
    {
        return $this->FIO;
    }
}

class Passenger extends Person implements ticket {

    protected $ticketNumber;
    protected $carNumber;
    protected $sitNumber;
    protected $luggage;

    function setTicketNumber($ticketNumber = null)
    {
        $this->ticketNumber = $ticketNumber;
    }

    function getTicketNumber()
    {
        return $this->ticketNumber;
    }

    function setCarNumber($carNumber)
    {
        $this->carNumber = $carNumber;
    }

    function getCarNumber()
    {
        return $this->carNumber;
    }

    function setSitNumber($sitNumber)
    {
        $this->sitNumber = $sitNumber;
    }

    function getSitNumber()
    {
        return $this->sitNumber;
    }

    function setLuggage($luggage)
    {
        $this->luggage = $luggage;
    }

    function getLuggage()
    {
        return $this->luggage;
    }

}

interface ticket
{
    function getTicketNumber();

    function getCarNumber();

    function getSitNumber();

    function getLuggage();
}


class Train extends Transport //implements trainTicket
{

    protected $type;
    private $passenger_2;

    public function setTransportType($type)
    {
        $this->type = $type;
        return $this;
    }



    public function takePassenger(Passenger $passenger_2)
    {
        $this->passenger_2 = $passenger_2;
        return $this;
    }

    public function checkTicket()
    {
        if ($this->passenger_2->getTicketNumber()){
            return true;
        }else{
            return false;
        }
    }

    public function ride()
    {
        $message = '';

        $a = $this->checkTicket();

        $l = $this->passenger_2->getLuggage();


        if (isset($a)){
            $message .= $this->passenger_2->Fio. "You can take your sit in the ".strtolower(get_class($this))." \n";

            if (isset($l)){
                $message .= "I'll pass your luggage into the ".strtolower(get_class($this))."'s luggage compartment. \n";
            }

            return $message;

        } else {
            $message .= "You can't take your sit in the ".strtolower(get_class($this))." \n";
            return $message;
        }

    }


}

$passenger_1 = new Passenger();
$passenger_1->setFio("Bandit_Banditovich_Ygolovchenko");
//$passenger_1->setTicketNumber(1);
$passenger_1->setCarNumber(2);
$passenger_1->setSitNumber(3);
//$passenger_1->setLuggage(1);



$train = new Train();
$train->takePassenger($passenger_1);


echo $train->ride();
