CLASS : Transport - environment: Air, color: Pink, mark: Boeing, model: 747, speed: 988, weight: 213
CLASS : Transport - environment: Air, color: Yellow, mark: Boeing, model: 747, speed: 988, weight: 213 - CHILD CLASS : Aircraft - swingspan: 68745, flightAltitude: 3562, engineCount: 4
CLASS : Transport - environment: Rails, color: White, mark: TEP, model: 70, speed: 200, weight: 130 - CHILD CLASS : Railway - commonTrainWeight: 730, fuel: Diesel


<?php
class Transport {

    protected $environment, $engineCount, $speed, $weight, $model, $mark;

    public function set_color($color){
        $this->color = $color;
        return $this;
    }

    public function set_weight($weight){
        $this->weight = $weight;
        return $this;
    }

    public function set_environment($environment){
            $this->environment = $environment;
            return $this;
        }

    public function set_mark($mark){
        $this->mark = $mark;
        return $this;
    }

    public function set_model($model){
        $this->model = $model;
        return $this;
    }

    public function set_speed($speed){
        $this->speed = $speed;
        return $this;
    }

    public function get_environment(){
        return $this->environment;
    }

    public function get_weight(){
            return $this->weight;
        }

    public function get_color(){
            return $this->color;
        }

    public function get_mark(){
        return $this->mark;
    }

    public function get_model(){
        return $this->model;
    }

    public function get_speed(){
        return $this->speed;
    }

    public function get_info(){
        $properties = ["environment", "color", "mark", "model", "speed", "weight"];
        $res = [];
        foreach ($properties as $prop) {
            $res[] = $prop.': '.$this->{"get_" . $prop}();
        }
        return "CLASS : ".__CLASS__." - ".implode(", ", $res);
    }

}


$air = new Transport();
$air
    ->set_color("Pink")
    ->set_environment("Air")
    ->set_weight(213)
    ->set_mark("Boeing")
    ->set_model("747")
    ->set_speed(988);

echo $air->get_info() . "\n";

class Aircraft extends Transport {
    protected $swingspan, $flightAltitude, $engineCount;

    public function set_swingspan($swingspan){
        $this->swingspan = $swingspan;
        return $this;
    }

    public function set_flightAltitude($flightAltitude){
        $this->flightAltitude = $flightAltitude;
        return $this;
    }

    public function set_engineCount($engineCount){
        $this->engineCount = $engineCount;
        return $this;
    }


    public function get_engineCount(){
        return $this->engineCount;
    }

    public function get_flightAltitude(){
        return $this->flightAltitude;
    }

    public function get_swingspan(){
        return $this->swingspan;
    }

    public function get_info(){
        $properties = ["swingspan", "flightAltitude", "engineCount"];
        $res = [];
        foreach ($properties as $prop) {
            $res[] = $prop.': '.$this->{"get_" . $prop}();
        }
        return parent::get_info()." - CHILD CLASS : ".__CLASS__." - ".implode(", ", $res);
    }


}

$airBus = new Aircraft();
$airBus
    ->set_color("Yellow")
    ->set_environment("Air")
    ->set_weight(213)
    ->set_mark("Boeing")
    ->set_model("747")
    ->set_swingspan(68745)
    ->set_flightAltitude(3562)
    ->set_engineCount(4)
    ->set_speed(988);

echo $airBus->get_info() . "\n";




class Railway extends Transport {
    protected $wagons, $wagonWeight, $fuel;

    public function set_wagons($wagons){
        $this->wagons = $wagons;
        return $this;
    }

    public function set_wagonWeight($wagonWeight){
        $this->wagonWeight = $wagonWeight;
        return $this;
    }

    public function set_fuel($fuel){
        $this->fuel = $fuel;
        return $this;
    }


    public function get_fuel(){
        return $this->fuel;
    }

    public function get_commonTrainWeight(){
        return $this->wagonWeight * $this->wagons + $this->weight;
    }


    public function get_info(){
        $properties = ["commonTrainWeight", "fuel"];
        $res = [];
        foreach ($properties as $prop) {
            $res[] = $prop.': '.$this->{"get_" . $prop}();
        }
        return parent::get_info()." - CHILD CLASS : ".__CLASS__." - ".implode(", ", $res);
    }


}

$train = new Railway();
$train
    ->set_color("White")
    ->set_environment("Rails")
    ->set_weight(130)
    ->set_mark("TEP")
    ->set_model("70")
    ->set_wagons(10)
    ->set_wagonWeight(60)
    ->set_fuel("Diesel")
    ->set_speed(200);

echo $train->get_info() . "\n";